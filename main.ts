export function calculateMaxStockProfit(input: Array<number>): number {
  let maxProfit = 0;

  // loop through the stock prices
  // current index will be the candidate of selling price
  for (let i = 1; i < input.length; i++) {
    // loop through the array from index = 0 to current index -1
    // get the minimal price
    // calculate the profit, if the profit is greater than the previous profit
    // then replace the maxProfit
    for (let j = 0; j < i - 1; j++) {
      const profit = input[i] - input[j];

      if (profit > maxProfit) {
        maxProfit = profit;
      }
    }
  }

  // return -1 if there is no profit
  if (maxProfit == 0) {
    return -1;
  }

  return maxProfit;
}
