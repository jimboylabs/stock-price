import { assertEquals } from "https://deno.land/std@0.204.0/assert/mod.ts";
import { calculateMaxStockProfit } from "./main.ts";

Deno.test("should return 5", () =>
  assertEquals(calculateMaxStockProfit([10, 12, 4, 5, 9]), 5));

Deno.test("should return -1", () =>
  assertEquals(calculateMaxStockProfit([10, 3, 2, 1]), -1));
